package tango.it.runner;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.tango.it.ITWithTangoDB;
import org.tango.it.TestDevice;
import org.tango.it.manager.DBDeviceManager;
import org.tango.server.testserver.JTangoTest;
import org.tango.utils.DevFailedUtils;

public class ITWithDBRunner {

    /**
     * boolean which define if we should perform cleaning after every test
     */
    protected static final boolean STOP_TEST_DEVICE_AFTER_EVERY_TEST = false;
    private static final String DEFAULT_DEVICE_NAME = "test/tango/jtangotest.1";
    public static Database tangoDatabase;
    private static ITWithTangoDB manager;

    @BeforeClass
    public static void setup() throws DevFailed {
        setup(new TestDevice(
                        DEFAULT_DEVICE_NAME,
                        JTangoTest.class,
                        JTangoTest.SERVER_NAME,
                        JTangoTest.INSTANCE_NAME
                )
        );
    }

    protected static void setup(TestDevice device) throws DevFailed {
        System.out.println("Tango host = " + System.getProperty("TANGO_HOST"));
        Assert.assertNotNull(System.getProperty("TANGO_HOST"));
        Assert.assertFalse(Boolean.parseBoolean(System.getProperty("org.tango.server.checkalarms")));
        try {
            init();
            manager.addDevice(device);
            manager.startTestDevice();
        } catch (DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    private static void init() throws DevFailed {
        if (manager == null) {
            manager = new DBDeviceManager();
            int retryNr = 3;
            do {
                retryNr--;
                try {
                    tangoDatabase = manager.getDatabase();
                } catch (DevFailed e) {
                    // retry connection
                    System.out.println("Retrying Tango database connection");
                    if (retryNr == 0) {
                        throw e;
                    }
                }
            } while (retryNr > 0);
        }
    }

    protected static boolean restartTestDevice() {
        try {
            manager.restartTestDevice();
            return true;
        } catch (DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            System.out.println("Can't restart device, exception is throw " + e);
            return false;
        }
    }

    @AfterClass
    public static void cleanup() throws DevFailed {
        if (!STOP_TEST_DEVICE_AFTER_EVERY_TEST) {
            manager.stopTestDevice();
        }
        manager.removeDevice();
    }

    protected DeviceProxy getDeviceProxy() throws DevFailed {
        try {
            return new DeviceProxy(getTestDeviceName());
        } catch (DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    protected static String getTestDeviceName() {
        return manager.getRunningDeviceName();
    }

    @Before
    public void startDevice() throws DevFailed {
        try {
            manager.startTestDevice();
        } catch (DevFailed e) {
            e.printStackTrace();
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @After
    public void stopDevice() throws DevFailed {
        if (STOP_TEST_DEVICE_AFTER_EVERY_TEST) {
            manager.stopTestDevice();
        }
    }
}
