package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithTangoDB;
import org.tango.it.TestDevice;
import tango.it.runner.ITWithDBRunner;


@Category(ITWithTangoDB.class)
public class DBDynamicAttributeTest extends ITWithDBRunner {

    private static final String deviceName = "tango/java/dynamic";

    @BeforeClass
    public static void setup() throws DevFailed {
        setup(new TestDevice(
                deviceName,
                DynamicAttrServer.class,
                "DynamicAttrServer",
                "1"
        ));
    }

    @Test
    public void testRemoveDynamicAttribute() throws DevFailed {
        String attributeName = "testProperAttributeRemoval";
        final DeviceProxy deviceProxy = getDeviceProxy();
        DeviceData inArg = new DeviceData();
        inArg.insert(attributeName);

        deviceProxy.command_inout("createDynamicAttribute", inArg);
        deviceProxy.command_inout("removeDynamicAttribute", inArg);
        deviceProxy.command_inout("removeAllDynamicAttribute");
    }

}
