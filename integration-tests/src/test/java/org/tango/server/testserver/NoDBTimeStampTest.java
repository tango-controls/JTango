package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import org.tango.utils.DevFailedUtils;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;


@Category(ITWithoutTangoDB.class)
public class NoDBTimeStampTest extends ITWithoutDBRunner {

    @Test
    public void test() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/booleanScalar");

        att.read();
        assertThat(att.getTimestamp(), equalTo(123456L));
    }

    @Test
    public void testReadTimestampDouble() throws DevFailed {
        try {
            final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/doubleScalar");

            att.read();
            final long time1 = att.getTimestamp();
            try {
                Thread.sleep(100);
            } catch (final InterruptedException e) {
            }
            att.read();
            final long time2 = att.getTimestamp();
            assertThat(time1, not(time2));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

}
