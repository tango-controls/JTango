package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoCommand;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBDeviceManagerTest extends ITWithoutDBRunner {

    @Test
    public void test() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(getDefaultDeviceFullName(), "getName");
        final String name = (String) cmd.executeExtract(null);
        assertThat(name, equalTo(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME));
    }

}
