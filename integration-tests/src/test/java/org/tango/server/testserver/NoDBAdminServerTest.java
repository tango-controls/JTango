package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import org.tango.server.PolledObjectType;
import org.tango.utils.DevFailedUtils;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBAdminServerTest extends ITWithoutDBRunner {

    @Test
    public void testQueryClass() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "QueryClass");
            cmd.execute();
            assertThat(cmd.extractToString(","), containsString(JTangoTest.class.getCanonicalName()));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testQueryDevice() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "QueryDevice");
            cmd.execute();
            assertThat(cmd.extractToString(","), containsString(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testQueryWizardDevProperty() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "QueryWizardDevProperty");
            cmd.execute(JTangoTest.class.getCanonicalName());
            assertThat(cmd.extractToString(","), containsString("myProp"));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testQueryWizardClassProperty() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "QueryWizardClassProperty");
            cmd.execute(JTangoTest.class.getCanonicalName());
            assertThat(cmd.extractToString(","), containsString("myClassProp"));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testRestartDevice() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "DevRestart");
            cmd.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME);
            new DeviceProxy(getDefaultDeviceFullName()).ping();
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testDevEmptyPollStatus() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "DevPollStatus");
            cmd.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME);
            System.out.println(cmd.extractToString(","));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    private void addAttPoll() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(getFullAdminName(), "AddObjPolling");
        final int[] param1 = new int[]{3000};
        final String[] param2 = new String[]{JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, PolledObjectType.ATTRIBUTE.toString(),
                "shortScalar"};
        cmd.insertMixArgin(param1, param2);
        cmd.execute();
    }

    private void remAttPoll() throws DevFailed {
        final TangoCommand rem = new TangoCommand(getFullAdminName(), "RemObjPolling");
        rem.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, PolledObjectType.ATTRIBUTE.toString(), "shortScalar");
    }

    @Test
    public void testAddPollingAttr() throws DevFailed {
        try {
            addAttPoll();
            final TangoAttribute attr = new TangoAttribute(getDefaultDeviceFullName() + "/shortScalar");
            attr.read();
            final TangoCommand status = new TangoCommand(getFullAdminName(), "DevPollStatus");
            status.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME);
            System.out.println(status.extractToString(","));
            remAttPoll();

        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testAddPollingCmd() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "AddObjPolling");
            final int[] param1 = new int[]{500};
            final String[] param2 = new String[]{JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, PolledObjectType.COMMAND.toString(),
                    "testPollingArray"};
            cmd.insertMixArgin(param1, param2);
            cmd.execute();
            final TangoCommand cmdPolled = new TangoCommand(getDefaultDeviceFullName() + "/testPollingArray");
            cmdPolled.execute();
            final TangoCommand rem = new TangoCommand(getFullAdminName(), "RemObjPolling");
            rem.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, PolledObjectType.COMMAND.toString(), "testPollingArray");

        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testDevPollStatus() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "DevPollStatus");
            cmd.execute(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME);
            System.out.println(cmd.extractToString(","));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testStopPolling() throws DevFailed {
        try {
            addAttPoll();
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "StopPolling");
            cmd.execute();
            remAttPoll();
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testStartPolling() throws DevFailed {
        try {
            addAttPoll();
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "StopPolling");
            cmd.execute();
            final TangoCommand cmd2 = new TangoCommand(getFullAdminName(), "StartPolling");
            cmd2.execute();
            remAttPoll();
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testLoggingLevel() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "SetLoggingLevel");
            final int[] param1 = new int[]{1};
            final String[] param2 = new String[]{JTangoTest.DEFAULT_NO_DB_DEVICE_NAME};
            cmd.insertMixArgin(param1, param2);
            cmd.execute();

            final TangoCommand cmd2 = new TangoCommand(getFullAdminName(), "GetLoggingLevel");
            final Object in = new String[]{JTangoTest.DEFAULT_NO_DB_DEVICE_NAME};
            cmd2.execute(in);
            final int result = cmd2.getNumLongMixArrayArgout()[0];
            assertThat(result, equalTo(param1[0]));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    // @Test need a device appender
    // public void testAddLogging() throws DevFailed {
    // try {
    // final TangoCommand cmd = new TangoCommand(getFullAdminName(), "AddLoggingTarget");
    // final Object in = new String[] { JTangoTest.noDbDeviceName,
    // "device::toto" };
    // cmd.execute(in);
    // } catch (final DevFailed e) {
    // DevFailedUtils.printDevFailed(e);
    // throw e;
    // }
    // }

    @Test
    public void testStopLogging() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "StopLogging");
            cmd.execute();
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testStartLogging() throws DevFailed {
        try {
            final TangoCommand cmd = new TangoCommand(getFullAdminName(), "StartLogging");
            cmd.execute();
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    // @Test
    // public void testRestartServer() throws DevFailed {
    // try {
    // final TangoCommand cmd = new TangoCommand(getFullAdminName(), "RestartServer");
    // cmd.execute();
    // } catch (final DevFailed e) {
    // DevFailedUtils.printDevFailed(e);
    // // throw e;
    // }
    //
    // try {
    // System.out.println("try connect");
    // System.out.println(new DeviceProxy(deviceName).status());
    // } catch (final DevFailed e) {
    // DevFailedUtils.printDevFailed(e);
    // // throw e;
    // }
    //
    // }

    // @Test(expected = DevFailed.class)
    // public void testKillServer() throws DevFailed {
    // try {
    // try {
    // Thread.sleep(1000);
    // } catch (final InterruptedException e1) {
    // // TODO Auto-generated catch block
    // e1.printStackTrace();
    // }
    // final TangoCommand cmd = new TangoCommand(getFullAdminName(), "Kill");
    // cmd.execute();
    // while (ServerManager.getInstance().isStarted()) {
    // try {
    // Thread.sleep(100);
    // } catch (final InterruptedException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }
    //
    // new DeviceProxy("tango://localhost:" + JTangoTest.noDbGiopPort + "/" +
    // JTangoTest.noDbDeviceName
    // + "#dbase=no").ping();
    // } catch (final DevFailed e) {
    // DevFailedUtils.printDevFailed(e);
    // throw e;
    // }
    // }
}
