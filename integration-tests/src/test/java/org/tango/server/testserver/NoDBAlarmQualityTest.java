package org.tango.server.testserver;

import fr.esrf.Tango.AttrQuality;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.client.database.DatabaseFactory;
import org.tango.client.database.ITangoDB;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsAnything.anything;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBAlarmQualityTest extends ITWithoutDBRunner {

    @BeforeClass
    public static void before() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();
        final String propName = "StateCheckAttrAlarm";
        final Map<String, String[]> map = new HashMap<>();
        map.put(propName, new String[]{"true"});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);
        new DeviceProxy(getDefaultDeviceFullName()).command_inout("Init");
    }

    @AfterClass
    public static void after() throws DevFailed {
        final ITangoDB db = DatabaseFactory.getDatabase();
        final String propName = "StateCheckAttrAlarm";
        final Map<String, String[]> map = new HashMap<>();
        map.put(propName, new String[]{"false"});
        db.setDeviceProperties(JTangoTest.DEFAULT_NO_DB_DEVICE_NAME, map);
        new DeviceProxy(getDefaultDeviceFullName()).command_inout("Init");
    }

    @Test
    public void testAlarmSpectrum() throws DevFailed {
        final String attrName = "longSpectrum";
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(new long[]{-10, 1});
        ta.read();
        // System.out.println(ta.getQuality().value());
        // System.out.println(DeviceState.toString(dev.state()));
        // System.out.println(dev.status());
        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_ALARM.value()));
        assertThat(dev.state(), equalTo(DevState.ALARM));
        assertThat(dev.status(), anything("Alarm : Value too low for longSpectrum"));
    }

    @Test
    public void testAlarmScalar() throws DevFailed {
        final String attrName = "doubleScalar";
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(-10.0);
        ta.read();
        // System.out.println(ta.getQuality().value());
        // System.out.println(DeviceState.toString(dev.state()));
        // System.out.println(dev.status());
        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_ALARM.value()));
        assertThat(dev.state(), equalTo(DevState.ALARM));
        assertThat(dev.status(), anything("Alarm : Value too low for doubleScalar"));
    }

    @Test
    public void testDeltaScalar() throws DevFailed {
        System.out.println("test deltaAttribute");
        final String attrName = "deltaAttribute";
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(-10.0);
        try {
            Thread.sleep(20);
        } catch (final InterruptedException e) {
        }
        // System.out.println(ta.getQuality().value());
        // System.out.println(DeviceState.toString(dev.state()));
        // System.out.println(dev.status());
        ta.read();
        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_ALARM.value()));
        assertThat(dev.state(), equalTo(DevState.ALARM));
        assertThat(dev.status(), anything("Alarm : RDS (R-W delta) for doubleScalar"));
    }

    @Test
    public void testWarning() throws DevFailed {
        final String attrName = "longSpectrum";
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(new long[]{4, 3});
        ta.read();
        // System.out.println(ta.getQuality().value());
        // System.out.println(DeviceState.toString(dev.state()));
        // System.out.println(dev.status());
        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_WARNING.value()));
        assertThat(dev.state(), equalTo(DevState.ALARM));
        assertThat(dev.status(), anything("Alarm : Value too high for longSpectrum"));

    }

    @Test
    public void testInvalid() throws DevFailed {
        final String attrName = "invalidQuality";
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        try {
            ta.read();
        } catch (final DevFailed e) {
            // ignore exception with quality invalid

        }
        // System.out.println(ta.getQuality().value());
        // System.out.println(DeviceState.toString(dev.state()));
        // System.out.println(dev.status());
        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_INVALID.value()));
        // assertThat(dev.state(), equalTo(DevState.ALARM));
        // assertThat(dev.status(), anything("Alarm : Value too high for longSpectrum"));

    }

    @Test
    public void testInvalid2() throws DevFailed {
        final String attrName = "invalidQuality2";
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        try {
            ta.read();
        } catch (final DevFailed e) {
            // ignore exception with quality invalid

        }

        assertThat(ta.getQuality().value(), equalTo(AttrQuality.ATTR_INVALID.value()));

    }

    @Ignore("min max on big array is not performant, fonctionnality removed")
    @Test(expected = DevFailed.class)
    public void testMaxValueSpectrum() throws DevFailed {
        final String attrName = "longSpectrum";
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(new long[]{-200, 120});
    }

    @Test(expected = DevFailed.class)
    public void testMaxValueScalar() throws DevFailed {
        final String attrName = "doubleScalar";
        final TangoAttribute ta = new TangoAttribute(getDefaultDeviceFullName() + "/" + attrName);
        ta.write(-200.0);
    }

}
