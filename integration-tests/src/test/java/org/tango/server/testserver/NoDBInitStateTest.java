package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


@Category(ITWithoutTangoDB.class)
public class NoDBInitStateTest extends ITWithoutDBRunner {

    @Test
    public void testInit() throws DevFailed {
        final TangoAttribute attr = new TangoAttribute(getDefaultDeviceFullName() + "/shortScalar");
        attr.write(20);
        final TangoCommand tangoCommand = new TangoCommand(getDefaultDeviceFullName(), "Init");
        tangoCommand.execute();
        final short shortScalar = (Short) attr.read();
        assertThat(shortScalar, equalTo((short) 10));
    }

    @Test
    public void testInitState() throws DevFailed {
        final TangoCommand tangoCommand = new TangoCommand(getDefaultDeviceFullName(), "Status");
        final Object status = tangoCommand.executeExtract(null);
        assertThat(status.toString(), equalTo("hello"));
    }

    @Test
    public void testState() throws DevFailed {
        final TangoCommand tangoCommand = new TangoCommand(getDefaultDeviceFullName(), "testState");
        tangoCommand.execute();
        final TangoCommand tangoCommand2 = new TangoCommand(getDefaultDeviceFullName() + "/State");
        final Object state = tangoCommand2.executeExtract(null);
        assertThat((DevState) state, equalTo(DevState.FAULT));
    }

    @Test
    public void testBlackBox() throws DevFailed {
        final DeviceProxy dev = new DeviceProxy(getDefaultDeviceFullName());
        dev.black_box(10);
    }

}
