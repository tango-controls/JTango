package org.tango.server.testserver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.tango.it.ITWithoutTangoDB;
import tango.it.runner.ITWithoutDBRunner;


@Category(ITWithoutTangoDB.class)
public class NoDBDevFailedTest extends ITWithoutDBRunner {

    @Test(expected = DevFailed.class)
    public void testErrorAttribute() throws DevFailed {
        final TangoAttribute att = new TangoAttribute(getDefaultDeviceFullName() + "/fduhfd");
        att.read();
    }

    @Test(expected = DevFailed.class)
    public void testErrorCommand() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(getDefaultDeviceFullName(), "fduhfd");
        cmd.execute();
    }

}
