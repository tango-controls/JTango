package org.tango.it;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;

public interface ITWithTangoDB {

    void addDevice(TestDevice deviceList) throws DevFailed;
    void removeDevice() throws DevFailed;
    String getRunningDeviceName();
    Database getDatabase() throws DevFailed;
    void startTestDevice() throws DevFailed;
    void stopTestDevice() throws DevFailed;
    void restartTestDevice() throws DevFailed;
}
