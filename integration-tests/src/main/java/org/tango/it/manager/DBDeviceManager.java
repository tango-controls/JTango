package org.tango.it.manager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import org.tango.it.ITWithTangoDB;
import org.tango.it.TestDevice;
import org.tango.server.ServerManager;

public class DBDeviceManager implements ITWithTangoDB {

    private boolean deviceIsRunning = false;
    private TestDevice device;

    @Override
    public void addDevice(TestDevice device) throws DevFailed {
        getDatabase().add_device(
                device.getDevName(),
                device.getDevClass().getCanonicalName(),
                device.getFullServerName()
        );
        this.device = device;
    }

    @Override
    public String getRunningDeviceName() {
        return device.getDevName();
    }

    @Override
    public Database getDatabase() throws DevFailed {
        return ApiUtil.get_db_obj();
    }

    @Override
    public void removeDevice() throws DevFailed {
        Database tangoDb = ApiUtil.get_db_obj();
        ServerManager.getInstance().stop();
        tangoDb.delete_device(device.getDevName());
        tangoDb.delete_server(device.getFullServerName());
    }

    @Override
    public void startTestDevice() throws DevFailed {
        if (!deviceIsRunning) {
            start();
            deviceIsRunning = true;
        }
    }

    private void start() throws DevFailed {
        ServerManager.getInstance().addClass(
                device.getDevClass().getCanonicalName(),
                device.getDevClass()
        );
        
        ServerManager.getInstance().startError(
                new String[]{device.getInstanceName()},
                device.getServName()
        );

        System.out.println(device.toString().concat(" was run successfully"));
    }

    @Override
    public void stopTestDevice() throws DevFailed {
        if (deviceIsRunning) {
            ServerManager.getInstance().stop();
            deviceIsRunning = false;
        }
    }

    @Override
    public void restartTestDevice() throws DevFailed {
        stopTestDevice();
        startTestDevice();
    }
}
