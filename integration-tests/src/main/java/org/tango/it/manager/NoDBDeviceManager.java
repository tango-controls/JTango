package org.tango.it.manager;

/**
 * Copyright (C) :     2012
 *
 * 	Synchrotron Soleil
 * 	L'Orme des merisiers
 * 	Saint Aubin
 * 	BP48
 * 	91192 GIF-SUR-YVETTE CEDEX
 *
 * This file is part of Tango.
 *
 * Tango is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tango is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import org.tango.it.ITWithoutTangoDB;
import org.tango.it.TestDevice;
import org.tango.server.Constants;
import org.tango.server.ServerManager;

import fr.esrf.Tango.DevFailed;

/**
 * Starts, stop the server JTangoTest without tango db
 *
 * @author ABEILLE
 *
 */
public class NoDBDeviceManager implements ITWithoutTangoDB {

    private List<String> deviceFullNameList = new ArrayList<>();
    private String adminName;

    @Override
    public void startDevicesForServer(TestDevice server, String deviceList) throws DevFailed, IOException {
        try (ServerSocket ss1 = new ServerSocket(0)) {
            ss1.setReuseAddress(true);
            ss1.close();

            System.setProperty("OAPort", String.valueOf(ss1.getLocalPort()));
            start(server, deviceList);
            
            saveDevicesFullNames(deviceList, ss1.getLocalPort());
            
            adminName = "tango://localhost:" + ss1.getLocalPort() + "/" + Constants.ADMIN_DEVICE_DOMAIN + "/"
                    + ServerManager.getInstance().getServerName() + "#dbase=no";
            for (String deviceFullName : deviceFullNameList) {
                System.out.println("START " + deviceFullName);
            }
        }
    }

    private void start(TestDevice deviceServer, String deviceList) throws DevFailed {
        ServerManager.getInstance().addClass(deviceServer.getDevClass().getCanonicalName(), deviceServer.getDevClass());
        ServerManager.getInstance().startError(new String[]{deviceServer.getInstanceName(), "-nodb", "-dlist", deviceList},
                deviceServer.getServName());
    }
    
    private void saveDevicesFullNames(String deviceList, int port) {
        for (String deviceName : deviceList.split(",")) {
            deviceFullNameList.add("tango://localhost:" + port + "/" + deviceName + "#dbase=no");
        }
    }

    @Override
    public String getDefaultDeviceFullName() {
        return deviceFullNameList.get(0);
    }

    @Override
    public List<String> getDeviceFullNameList() {
        return deviceFullNameList;
    }

    @Override
    public String getFullAdminName() {
        return adminName;
    }

    @Override
    public void stopDevices() throws DevFailed {
        ServerManager.getInstance().stop();
        clean();
    }

    protected void clean() {
        deviceFullNameList = new ArrayList<>();
    }

}

