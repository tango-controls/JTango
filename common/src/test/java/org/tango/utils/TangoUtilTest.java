package org.tango.utils;

import fr.esrf.Tango.DevFailed;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TangoUtilTest {

    @Test
    public void getFullDeviceNameForCommand() {
        try {
            TangoUtil.checkFullCommandName("tango://172.17.0.3:10000/test/debian8/10/IOState");
            TangoUtil.checkFullCommandName("tango://test/debian8/10/IOState");
            TangoUtil.checkFullCommandName("172.17.0.3:10000/test/debian8/10/IOState");
            TangoUtil.checkFullCommandName("localhost:10000/test/debian8/10/IOState");
            TangoUtil.checkFullCommandName("test/debian8/10/IOState");
        } catch (DevFailed e) {
            fail("Exception should not be thrown");
        }

        String command = "";
        try {
            command = "tango://172.17.0.3:10000/test/debian8/10/";
            TangoUtil.checkFullCommandName(command);
            fail("Exception should be thrown");
        } catch (DevFailed e) {
            assertEquals(command + " command must contain 4 fields", e.getLocalizedMessage());
        }

        try {
            command = "test/debian8/10";
            TangoUtil.checkFullCommandName(command);
            fail("Exception should be thrown");
        } catch (DevFailed e) {
            assertEquals(command + " command must contain 4 fields", e.getLocalizedMessage());
        }
    }

}