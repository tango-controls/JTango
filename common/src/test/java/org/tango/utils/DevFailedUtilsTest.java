package org.tango.utils;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.tango.utils.DevFailedUtils.buildDevError;

public class DevFailedUtilsTest {

    @Test
    public void buildDevErrorFull() {
        DevError[] er = buildDevError(new DevFailed("reason1",
                        buildDevError("reason1", "description1", 0)),
                        "reason2", "description2", ErrSeverity.WARN, 0);
        assertEquals(2, er.length);
        assertEquals(ErrSeverity.WARN, er[1].severity);
    }

    @Test
    public void buildDevError2() {
        DevError[] er = buildDevError("reason", "description", 0);
        assertEquals(1, er.length);
        assertEquals(ErrSeverity.ERR, er[0].severity);
    }
}