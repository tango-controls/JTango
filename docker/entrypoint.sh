#!/bin/bash

echo "Running the command java -DTANGO_HOST='${TANGO_HOST}' -Dlogback.configurationFile=logback.xml -cp '${DEVICE_SERVER_CLASS}' to start the Device Server"


java -DTANGO_HOST="${TANGO_HOST}" -Dlogback.configurationFile=logback.xml -cp JTangoServer.jar "${DEVICE_SERVER_CLASS}"
